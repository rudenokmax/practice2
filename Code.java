// Абстрактная фабрика
interface AbstractFactory {
    Unit createUnit();
    Building createBuilding();
}

// Фабрика юнитов
class UnitFactory implements AbstractFactory {
    public Unit createUnit() {
        return null;
    }

    public Building createBuilding() {
        return null;
    }
}

// Фабрика зданий
class BuildingFactory implements AbstractFactory {
    public Unit createUnit() {
        return null;
    }

    public Building createBuilding() {
        return null;
    }
}

// Классы для создания конкретных объектов
interface Unit {}

class InfantryUnit implements Unit {
    public void draw() {
        System.out.println("Infantry unit created");
    }
}

class TankUnit implements Unit {
    public void draw() {
        System.out.println("Tank unit created");
    }
}

class AircraftUnit implements Unit {
    public void draw() {
        System.out.println("Aircraft unit created");
    }
}

interface Building {}

class Barracks implements Building {
    public void draw() {
        System.out.println("Barracks building created");
    }
}

class Factory implements Building {
    public void draw() {
        System.out.println("Factory building created");
    }
}

class Headquarters implements Building {
    public void draw() {
        System.out.println("Headquarters building created");
    }
}

// Класс тайла на карте
class Tile {
    private int x;
    private int y;
    
    public Tile(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
